<?php
	include ("./config.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Chat Tutorial</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="style.css">

<script>

$(document).ready(function (e) {

	function displayChat() {

	$.ajax({

		url:'displayChat.php  ',
		type: 'POST',
		success: function(data) {
			$("#chatDisplay").html(data);
		}


		});

	}
	
	setInterval(function() {displayChat();}, 1000);

	

	$('#sendmessageBtn').click(function(e) {

	var name = $("#user_name").val();
	var Message = $("#message").val();
	$("#myChatForm")[0].reset();

		$.ajax({

			url:'sendchat.php',
			type:'POST',
			data:{
				uname:name,
				umessage:Message
			}

		});

	});
});

</script>

</head>

<body>
<div class="container-fluid">
	<h3 class="text-center">Live Chat Room</h3>

	<div class="well" id="chatBox">

	<div id="chatDisplay"></div> 

</div>

	<form id="myChatForm">

	<input type="text" id="user_name" placeholder="Enter your name:"><br>
	<textarea name="message" id="message" placeholder="Enter your Message here..." cols="38" rows="3"></textarea><br>
	<button  type="button" class="btn btn-success btn-lg" id="sendmessageBtn">Send Message</button>
	</form>

</div>


</body>
</html>
